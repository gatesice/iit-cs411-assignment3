// ========================================================
// CS411 Assignment 3 - Tetris
// Author : Lu Wang
// CWID : A20315534
// ========================================================

#include <stdlib.h>
#include <iostream>
#include <math.h>
#include <time.h>
#include "GL/glut.h"

// ### Type Definition ### //

typedef enum {NORMAL, HIGH, LOW} colorType;

typedef struct{
  float x,y;
} Point;

typedef struct{
  Point location;       // current location of piece
  float rotAng;         // current rotation of piece (degrees)
  int   color;          // we use indexed color; use the color function.
  int   numBlocks;      // the number of occupied blocks in this piece
  Point blockList[15];  // a list of occupied blocks for this piece
} Piece;

typedef float Mat3[3][3]; //a 3x3 'matrix'

// Constants
const float PI  = 3.141592653589793;
int playW = 12;   // playboard width
int playH = 22;   // playboard height
const int playWMax = 40;
const int playHMax = 72;
const int pieceW = 5;   // piece width
const int pieceH = 3;   // piece height

// Global Variables
Piece pieceList[25];    // list of possible pieces
int pieceNum = 0;       // total number of possible pieces
Piece *curPiece;        // The piece currently in play
int playfield[playWMax][playHMax]; // The playing field - contains zeros for blank
                             // spots, the color index otherwise.
int dropDelay = 1000;   // number of milliseconds between piece drops
int lastDropTime = 0;	// used to control next auto drop time.
int lines = 0;          // number of lines obtained
int score = 0;          // current score
int shadowFlag=1;       // controlls shadow display
bool needNewPieceFlat = false;	// note that if it needs to pop a new piece

enum GameStatus { RUNNING, STOPPED };
int status = GameStatus::STOPPED;

// ### Debug Outputs ### //

void debug_ShowCurPieceInTitle() {
	char titleStr[256];
	sprintf(titleStr, "curPiece: (%d,%d) rot(%d)", 
		(int)(curPiece->location.x + 0.5), 
		(int)(curPiece->location.y + 0.5), 
		(int)(curPiece->rotAng));
	glutSetWindowTitle(titleStr);
}

// ### File I/O ### //

void readPieceLst(char *fn, Piece *list)
{
	FILE* fin;
	char c;
	Piece *p;
	int i,j,k;
	int check;
	int centerX = int(pieceW/2);
	int centerY = int(pieceH/2);

	// open input file
	if(! (fin=fopen(fn,"r")) ){
		printf("\nCan't open file %s\n\7",fn);
		return;
	}

	// read # of pieces
	fscanf(fin,"%d",&pieceNum);
	while(fread(&c,1,1,fin)) if (c== '\n') break;

	for(i=0; i < pieceNum; i++){
		p = &pieceList[i];
		p->location.x = p->location.y = p->rotAng = p->color = p->numBlocks = 0;
    
		// read a piece
		for(j=0; j < pieceH; j++) for(k=0; k < pieceW; k++){
			fscanf(fin, "%d", &check);
			if(check == 1){
			p->blockList[p->numBlocks].x = k-centerX;
			p->blockList[p->numBlocks].y = j-centerY;
			p->numBlocks++;
			}
		}
	}

	// return number of objects read
	fclose(fin);
}

void dumpPiece(Piece *p){
	int j;
	printf("  numBlocks:\t%d\n",p->numBlocks);
	printf("  Block List:\t");            
	for(j=0;j<p->numBlocks;j++){     
		printf("(%1.0f,%1.0f) ",p->blockList[j].x, p->blockList[j].y);
	}
	printf("\n\n");
}

void dumpWholePiece(Piece *p){
  printf("  Location:\t(%d,%d)\n",p->location.x,p->location.y);
  printf("  Angle:\t%d\n",p->rotAng);
  printf("  Color:\t%d\n",p->color);
  dumpPiece(p);
}

void dumpPieceLst(){
  Piece *p;
  int i;
  for(i=0; i<pieceNum; i++){
    p = &pieceList[i];
    printf("Piece %d:\n",i);
    dumpPiece(p);
  }
}

// ### Matrix Functions ### //

//multiplies a point 'p' by a 3x3 matrix 'm' in homogeneous
//coordinates. The result is stored in 'p'
void matMult(Point *p, Mat3 m){
	// Convert point 'p' to homogeneous coordinates
	float hp[3] = {p->x, p->y, 1}, 
		hpResult[3] = {0, 0, 0};

	// Calculate Matrix
	for(int i = 0; i < 3; i ++) {
		for (int j = 0; j < 3; j ++) {
			hpResult[i] += hp[j] * m[i][j];
		}
	}
	
	// Convert back the homogeneous coordinates to point 'p'.
	if (hpResult[2] != 0) {
		p->x = hpResult[0]/hpResult[2];
		p->y = hpResult[1]/hpResult[2];
	} else {
		printf("Error! Try to divide by 0\n");
	}
}

// rotate a point by 'p' an angle 'theta' (degrees) about the origin.
// The result is stored in 'p'
void pRotate(Point *p, float theta){
	float angle = theta * PI / 180;
	// Initialize a matrix for transforming
	Mat3 mR = {
		cos(angle),		- sin(angle),	0,
		sin(angle),		cos(angle),		0,
		0,				0,				1 };
	matMult(p, mR);
}

// translate a point 'p' by '(x,y)'. The result is stored in 'p'
// Just simply add the 'x' and 'y' coordinates
void pTranslate(Point *p, float x, float y){
	Mat3 mR = {
		1,	0,	x,
		0,	1,	y,
		0,	0,	1};
	matMult(p, mR);
}

// ### Board Management ### //

// check if 'piece' can move by (dx, dy)
int canMove(Piece *piece, int dx, int dy, bool showInfo = true) {
	Point p;

	for(int i = 0; i < piece->numBlocks; i ++) {
		p = piece->blockList[i];

		pRotate(&p, piece->rotAng);
		pTranslate(&p, piece->location.x + dx, piece->location.y + dy);

		// Bound Check
		if (p.x < 0 || p.y < 0) {
			printf("[ERROR] Out of boundary in canMove()\n");
			return 0;
		}

		// Overlap Check
		if (playfield[int(p.x + 0.5)][int(p.y + 0.5)] >= 0) {
			if(showInfo) { 
				printf("[INFO] The block (%d,%d) is overlap with other blocks in canMove()\n", 
					int(p.x + 0.5), 
					int(p.y + 0.5)); 
			}
			return 0;
		}
	}

	return 1;
}

int canRotate(Piece *piece, int theta, bool showInfo = true) {
	Point p;

	for(int i = 0; i < piece->numBlocks; i ++) {
		p = piece->blockList[i];

		pRotate(&p, piece->rotAng + 90);
		pTranslate(&p, piece->location.x, piece->location.y);

		// Bound Check
		if (p.x < 0 || p.y < 0) {
			printf("[ERROR] Out of boundary in canRotate()\n");
			return 0;
		}

		// Overlap Check
		if (playfield[int(p.x + 0.5)][int(p.y + 0.5)] >= 0) {
			if(showInfo) {
				printf("[INFO] The block (%d,%d) is overlap with other blocks in canRotate()\n", 
					int(p.x + 0.5), 
					int(p.y + 0.5));
			}
			return 0;
		}
	}

	return 1;
}

// write the current piece to the playfield
void deposit() {
	Point p;

	for (int i = 0; i < curPiece->numBlocks; i ++) {
		p = curPiece->blockList[i];

		pRotate(&p, curPiece->rotAng);
		pTranslate(&p, curPiece->location.x, curPiece->location.y);

		// Update the playfield at the computed coordinates
		playfield[int(p.x + 0.5)][int(p.y + 0.5)] = curPiece->color;
	}
}

// move the piece by specified amount
void movePiece(int x, int y) {
	curPiece->location.x += x;
	curPiece->location.y += y;
}

void initBoard() {
	int i; 
	memset(playfield, -1, sizeof(playfield));

	// draw the borders
	for(i = 0; i < playH; i ++) {
		playfield[0][i] = 0;
		playfield[playW-1][i] = 0;
	}
	for(i = 0; i < playW; i ++) {
		playfield[i][0] = 0;
	}
}

void dropDownLines(int line) {
	for(int i = line; i < playH - 1; i ++) {
		for(int j = 1; j < playW - 1; j ++) {
			playfield[j][i] = playfield[j][i+1];
		}
	}
}

// Check and delete the filled lines.
void checkLines() {
	int line, mult = 1;

	for(int i = 1; i < playH; i ++) {
		line = 1;
		// identify a completed line
		for(int j = 0; j < playW; j ++) {
			if (playfield[j][i] == -1) {
				line = 0;
				break;
			}
		}

	// delete a completed line
		if (line == 1) {
			lines ++;
			if(lines % 10 == 0) {
				dropDelay = int (dropDelay * 0.8);
			}
			score += 10 * mult;
			mult ++;
			dropDownLines(i);
			i --;
		}
	}
}

// Pop a new piece in to game board.
void popNewPiece() {
	int i = rand() % pieceNum;
	curPiece -> location.x = int((playW - 2) / 2);
	curPiece -> location.y = playH - 3;
	curPiece -> rotAng = (rand() % 4) * 90;
	curPiece -> color = (rand() % 7) + 1;
	curPiece -> numBlocks = pieceList[i].numBlocks;

	for(int j = 0; j < curPiece->numBlocks; j ++) {
		curPiece->blockList[j].x = pieceList[i].blockList[j].x;
		curPiece->blockList[j].y = pieceList[i].blockList[j].y;
	}

	if (!canMove(curPiece, 0, 0)) {
		printf("[INFO] Game Over!!!\n");
		glutIdleFunc(NULL);
		status = GameStatus::STOPPED;
	}
}

// When press <down> or the drop timer ticked, the piece should go down for one block
// and check if the piece is touched the bottom.
void go(int dx, int dy) {
	// Check if current piece is placed
	if (dx == 0 && dy == -1 &&
		canMove(curPiece, 0, 0, false) &&
		!canMove(curPiece, 0, -1, false)) {
		deposit();
		checkLines();
		lastDropTime = dropDelay - 1;
		needNewPieceFlat = true;
		glutPostRedisplay();
	}

	if (canMove(curPiece, dx, dy)) {
		movePiece(dx, dy);
		glutPostRedisplay();
	} else {
		printf("[INFO] Can't move current piece by (%d, %d)\n", dx, dy);
	}
}

// When press <space>, current piece will go straight down as far as it can.
// The piece will also merged into the gameboard then pop the next piece.
void goDownToEnd() {
	while (canMove(curPiece, 0, -1)) {
		movePiece(0, -1);
	}
	deposit();
	checkLines();
	lastDropTime = dropDelay - 1;
	needNewPieceFlat = true;
	glutPostRedisplay();
}

// ### Drawing Functions ### //


//print a text string
void renderStrokeString(char *string)
{
	glPushMatrix();
	glScalef(.0085,.0085,.0085);
	char *c;
	// loop over all the characters in the string
	for (c=string; *c != '\0'; c++) {
		glutStrokeCharacter(GLUT_STROKE_ROMAN, *c);
	}
	glPopMatrix();
}

//set the current color using a color index (0-8). Use type for highlights
void setColor(int color, colorType type){
	float on, off;
	float r,g,b;
	switch (type){
		case NORMAL:on = 1.0f;      off = 0.333f;   break;
		case HIGH:  on = 1.0f;      off = 0.666f;   break;
		case LOW:   on = 0.666f;    off = 0.333f;   break;
	}

	//index 0-7 (xrgb): x000 x001 x010 x011 x100 x101 x110 x111
	//when the x bit is 1 reduce intensity by a factor of 4 
	if (color & 4) r = on; else r = off;
	if (color & 2) g = on; else g = off; 
	if (color & 1) b = on; else b = off; 
	if (color & 8) {r/=4; g/=4; b/=4;}

	//and finally set the color!
	glColor3f(r,g,b);
}

//draw a single block with a specified color
void drawBlock(int color){
	glPushMatrix();
	glTranslatef(-0.5,-0.5,0.0);
	glColor3f(0.25,0.25,0.25);
	glRectf(0.0,0.0,1.0,1.0);

	//make it all 3-d(ish)
	glBegin(GL_TRIANGLES);
	setColor(color,HIGH);
		glVertex2f(0.1,0.1);
		glVertex2f(0.9,0.9);
		glVertex2f(0.1,0.9);
	setColor(color,LOW);
		glVertex2f(0.1,0.1);
		glVertex2f(0.9,0.1);
		glVertex2f(0.9,0.9);
	glEnd();

	setColor(color,NORMAL);
	glRectf(0.2,0.2,0.8,0.8);
	glPopMatrix();
}

//draw a complete piece at the specified coordinates USING OPENGL
void drawPiece(Piece *p){
	int i;
	Point disp;

	// loop over all the blocks
	for(i=0;i<p->numBlocks;i++){
	disp = p->blockList[i];

	glPushMatrix();
		glTranslatef(p->location.x,p->location.y,0.0);
		glRotatef(p->rotAng,0,0,1);
		glTranslatef(disp.x,disp.y,0.0);
		drawBlock(p->color);
	glPopMatrix();
	}
}

//draw a shadow piece at the bottom
void drawShadow(){
	Piece shadow = *curPiece;
	shadow.color|=8;
	while(canMove(&shadow,0,-1, false)) { shadow.location.y -= 1; }
	drawPiece(&shadow);  
}

//display score, level, etc.
void displayScore(int score, int lines){
	char temp[25];
	glPushMatrix();
		glColor3f(1.0,1.0,1.0);
		glTranslatef(playW+.1,playH-3,0.0);
		renderStrokeString("TETRIS");
		glTranslatef(0.0,-4.0,0.0);
		renderStrokeString("score");
		glTranslatef(0.5,-1.2,0.0);
		sprintf(temp,"%d", score);
		renderStrokeString(temp);
		glTranslatef(-0.5,-2.0,0.0);
		renderStrokeString("lines");
		glTranslatef(0.5,-1.2,0.0);
		sprintf(temp,"%d", lines);
		renderStrokeString(temp);
		glTranslatef(-0.5,-2.0,0.0);
		renderStrokeString("level");
		glTranslatef(0.5,-1.2,0.0);
		sprintf(temp,"%d", lines/10);
		renderStrokeString(temp);
	glPopMatrix();
}

void display(void){
	int i,j;
	glClear (GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);

	//display the current score
	displayScore(score,lines);

	//display the playfield - borders are contained in it
	for(i=0;i<playW;i++){
		for(j=0;j<playH;j++){
			if(playfield[i][j] >= 0){
				glPushMatrix();
				glTranslatef(i,j,0.0);
					drawBlock(playfield[i][j]);
				glPopMatrix();
			}
		}
	}

	//draw the current piece
	if (curPiece->numBlocks > 0) {
		if(canMove(curPiece,0,0)){
			if(shadowFlag) { drawShadow(); }
			drawPiece(curPiece);
		}
	}
	glutSwapBuffers();
	debug_ShowCurPieceInTitle();
}


// ### Program Entrance ### //


void init(void)
{
	time_t buff[10];
	glClearColor (0.0, 0.0, 0.0, 0.0);
	glShadeModel (GL_FLAT);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glMatrixMode(GL_PROJECTION);
	gluOrtho2D(-0.5,playW+4,-0.5,playH-2+.5);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	//initialize the playfield
	initBoard();

	srand(time(buff));
	curPiece = (Piece*)malloc(sizeof(Piece));
	status = GameStatus::RUNNING;
	needNewPieceFlat = true;
}     

void reshape (int w, int h)
{
  //maintain a fixed aspect ratio of 4:5
  if(w/h > 4.0/5.0) glViewport (0, 0, int(h*(4.0/5.0)), h);
  else glViewport (0, 0, w, int(w*(5.0/4.0)) );
}

void idle(void){
	if (lastDropTime < 0) {
		lastDropTime = glutGet(GLUT_ELAPSED_TIME);
	}
	int curTime = glutGet(GLUT_ELAPSED_TIME);
	int elapsed = curTime - lastDropTime;

	if(elapsed <= dropDelay) return;
	lastDropTime = curTime;
	if (status == GameStatus::RUNNING) {
		if(needNewPieceFlat) { popNewPiece(); needNewPieceFlat = false; }
		else { go(0, -1); }
	}
	glutPostRedisplay();
}

void help()
{
  printf("Keys:\n");
  printf("-----\n");
  printf("left/right/down arrows - move\n");
  printf("up arrow - rotate\n");
  printf("space - drop immediately \n");
  printf("ESC - quit\n");
}

void keyboard(unsigned char key, int x, int y)
{
  int i;

  switch(key){
    case 27: exit(0); //ESC
    case 'h': help(); break;
	case ' ': if (status == GameStatus::RUNNING) { goDownToEnd(); } 
			  break;
  }
}

void special(int key, int x, int y){
	switch (key)
	{
	case GLUT_KEY_UP: // Rotate piece 
		if (status == GameStatus::RUNNING && canRotate(curPiece, 90)) {
			curPiece->rotAng = (float)((int)(curPiece->rotAng + 90) % 360);
			glutPostRedisplay();
		}
		break;
	case GLUT_KEY_DOWN: // drop piece for one block
		if (status == GameStatus::RUNNING && canMove(curPiece, 0, -1)) {
			go(0, -1);
			glutPostRedisplay();
		}
		break;
	case GLUT_KEY_LEFT: // move piece left
		if (status == GameStatus::RUNNING && canMove(curPiece, -1, 0)) {
			go(-1, 0);
			glutPostRedisplay();
		}
		break;
	case GLUT_KEY_RIGHT: // move piece right
		if (status == GameStatus::RUNNING && canMove(curPiece, 1, 0)) {
			go(1, 0);
			glutPostRedisplay();
		}
		break;
	default:
		break;
	}
}


int main(int argc, char *argv[])
{
	// check transferred arguments
	if(argc<2){
		printf("\nUsage: tetris <pieceFile> [<width> <height>]\n\n");
		exit(0);
	}
	if(argc > 2) {
		playW = atoi(argv[2]);   // playboard width
		playW = playW > playWMax ? playWMax : playW;
		playH = atoi(argv[3]);   // playboard height
		playH = playH > playHMax ? playHMax : playH;
	}

	// load, dump, and process the object file
	readPieceLst(argv[1],pieceList);
	dumpPieceLst();

	glutInit(&argc, argv);
	glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGB);
	glutInitWindowSize (480, 600);
	glutInitWindowPosition (100, 100);
	glutCreateWindow (argv[0]);
	init ();
  
	glutDisplayFunc(display);
	glutReshapeFunc(reshape);
	glutKeyboardFunc(keyboard);
	glutSpecialFunc(special);
	glutIdleFunc(idle);
	glutMainLoop();
	return 0;
}
